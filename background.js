var scripts = [
  'jquery.min.js',
  'clipboard.min.js',
  'content_script.js'
];

function findSSR(tab) {
	scripts.forEach(function(script) {
  chrome.tabs.executeScript(tab.id, { file: script }, function(resp) {
    if (script!=='content_script.js') return;
  });
});
};

chrome.browserAction.onClicked.addListener(findSSR)